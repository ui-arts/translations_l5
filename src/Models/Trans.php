<?php

namespace Vis\Translations;

use Yandex\Translate\Translator as Translator;
use JoggApp\GoogleTranslate\GoogleTranslate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;

class Trans extends Model
{
    protected $table = 'translations_phrases';

    public static $rules = [
        'phrase' => 'required|unique:translations_phrases',
    ];

    protected $fillable = ['phrase'];

    public $timestamps = false;

    public function getTrans()
    {
        $res = $this->hasMany('Vis\Translations\Translate', 'id_translations_phrase')->get()->toArray();

        if ($res) {
            $trans = [];
            foreach ($res as $k=>$el) {
                $trans[$el['lang']] = $el['translate'];
            }

            return $trans;
        }
    }

    /**
     * auto generate translation for function __() if empty.
     *
     * @param string $phrase
     * @param strign $thisLang
     *
     * @return string
     */
    public static function generateTranslation($phrase, $thisLang)
    {
        if ($phrase && $thisLang) {
            $checkPresentPhrase = self::where('phrase', 'like', $phrase)->first();
            if (! isset($checkPresentPhrase->id)) {
                $newPhrase = self::create(['phrase' => $phrase]);

                $langsDef = Config::get('translations.config.def_locale');
                $langsAll = Config::get('translations.config.alt_langs');

                foreach ($langsAll as $lang) {
                    //to not translate default language
                    if ($lang == 'ru') {
                        Translate::create(
                            [
                                'id_translations_phrase' => $newPhrase->id,
                                'lang'                   => str_replace('uk', 'ua', $lang),
                                'translate'              => $phrase,
                            ]
                        );
                        continue;
                    }

                    $lang = str_replace('ua', 'uk', $lang);
                    $langsDef = str_replace('ua', 'uk', $langsDef);
                    $translate = $phrase;

                    if (config('googletranslate.api_key')) {
                        $translate = \GoogleTranslate::justTranslate($phrase, $lang);
                    }

                    Translate::create(
                        [
                            'id_translations_phrase' => $newPhrase->id,
                            'lang'                   => str_replace('uk', 'ua', $lang),
                            'translate'              => $translate,
                        ]
                    );
                }

                self::reCacheTrans();
                $arrayTranslate = self::fillCacheTrans();

                return $arrayTranslate[$phrase][$thisLang] ?? 'error translation';
            }

            $translatePhrase = Translate::where('id_translations_phrase', $checkPresentPhrase->id)
                ->where('lang', 'like', $thisLang)->first();

            return $translatePhrase ? $translatePhrase->translate : $phrase;
        }
    }

    /**
     * filling cache translate.
     *
     * @return array
     */
    public static function fillCacheTrans()
    {
        if (Cache::get('translations')) {
            $arrayTranslate = Cache::get('translations');
        } else {
            $arrayTranslate = self::getArrayTranslation();
            Cache::forever('translations', $arrayTranslate);
        }

        return $arrayTranslate;
    }

    /** recache translate
     *
     * @return void
     */
    public static function reCacheTrans()
    {
        Cache::forget('translations');
        self::fillCacheTrans();
    }

    /**
     * get array all phrase translation.
     *
     * @return array
     */
    private static function getArrayTranslation()
    {
        $translationsGet = DB::table('translations_phrases')
            ->leftJoin('translations', 'translations.id_translations_phrase', '=', 'translations_phrases.id')
            ->get(['translate', 'lang', 'phrase']);

        $arrayTranslate = [];
        foreach ($translationsGet as $el) {
            $el = (array) $el;
            $arrayTranslate[$el['phrase']][$el['lang']] = $el['translate'];
        }

        return $arrayTranslate;
    }
}
